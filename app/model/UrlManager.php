<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 5.6.18
 * Time: 12:42
 */

namespace App\Model;

use Nette;
use Nette\Utils\ArrayHash;
use Nette\Utils\Random;
/**
 * Class EmailManager třída modelu pro správu tabulky sm_url.
 * Dědí abstraktní model a získává tím přístu do DB.
 * @package App\Model
 */
class UrlManager extends BaseManager
{
    use Nette\SmartObject;

    /** Konstanty pro manipulaci s modelem. */
    const
        TABLE_NAME = 'sm_url',
        COLUMN_ID = 'id',
        COLUMN_URL = 'url_hash',
        COLUMN_DATE = 'datum',
        VALID_DUR = 14; // počet dní (platnost hash url)


    /**
     * v případě potřeby, zajišťuje vytváření nového platného hash pro url
     * @return string existing | created
     */
    public function createNewUrl()
    {
        $lastRow = $this->getNewestNumRows();

        if ($lastRow > 0){
            return 'existing';
        }else{
            $this->add([
                    self::COLUMN_URL => sha1(Random::generate(60)),
                    self::COLUMN_DATE => date('Y-m-d')
                ]
            );
            return 'created';
        }
    }

    /**
     * Pro získání počtu validních hash url. Očekává maximálně jeden validní nebo žádný.
     * @return int počet záznamů
     */
    public function getNewestNumRows()
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_DATE . ' > ' . "DATE_SUB(NOW(), INTERVAL " . self::VALID_DUR . " DAY)")
            ->order(self::COLUMN_DATE . ' ASC')
            ->count();
    }

    /**
     * Kontroluje validaci vstupního parametru $id
     * @param $id ?|string očekává se hash URL předaným presenterem
     * @return int počet výsledků
     */
    public function getValidUrl($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COLUMN_DATE . ' > ' . "DATE_SUB(NOW(), INTERVAL " . self::VALID_DUR . " DAY)")
            ->where(self::COLUMN_URL, $id)
            ->order(self::COLUMN_DATE . ' ASC')
            ->count();
    }

    /**
     * Vrátí řádek z tabulky TABLE_NAME v databázi
     * @param $id ? hodnota sloupce
     * @param string $col nazav sloupce
     * @param string $order řadit podle // zbytečný
     * @return Nette\Database\Table\ActiveRow aktivnvní řádek
     */
    public function getThisRow($id, $col = self::COLUMN_ID, $order = self::COLUMN_ID . ' DESC') //:array
    {
        return $this->database->table(self::TABLE_NAME)
            ->where($col, $id)
            ->order($order)
            ->fetch();
    }

    /**
     * Vrátí seznam řádků (emailů a jejich zpráv) z tabulky TABLE_NAME v databázi
     * @param string $order
     * @param ? $where
     * @return Nette\Database\Table\Selection seznam řádků
     */
    public function getAllRows($order = self::COLUMN_ID . ' DESC', $where = 0) //:array
    {
        return $this->database->table(self::TABLE_NAME)
            ->where($where)
            ->order($order)
            ->fetchAll();
    }

    /**
     * Vloží nový záznam do databáze.
     * @param array|ArrayHash $zaznam email
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function add($zaznam) //:bool
    {
        //dát sem jak insert tak update
        //pokud je $zaznam['id'] nastaven, tak edituj
        return $this->database->table(self::TABLE_NAME)->insert([$zaznam]);
    }

}