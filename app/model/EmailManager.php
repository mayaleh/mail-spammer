<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 22.5.18
 * Time: 11:16
 */

namespace App\Model;
use Nette;
use Nette\Database\Table\IRow;
use Nette\Database\Table\Selection;
use Nette\Utils\ArrayHash;

/**
 * Class EmailManager třída modelu pro správu tabulky sm_emails.
 * Dědí abstraktní model a získává tím přístu do DB.
 * @package App\Model
 */
class EmailManager extends BaseManager
{
    use Nette\SmartObject;

    /** Konstanty pro manipulaci s modelem. */
    const
        TABLE_NAME = 'sm_emails',
        COLUMN_ID = 'id',
        COLUMN_EMAIL = 'email',
        COLUMN_MESSAGE = "zprava";


    /**
     * Vloží nový záznam do databáze.
     * @param array|ArrayHash $zaznam email
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function add($zaznam) //:bool
    {
        //dát sem jak insert tak update
        //pokud je $zaznam['id'] nastaven, tak edituj
        return $this->database->table(self::TABLE_NAME)->insert([$zaznam]);
    }

    /**
     * Vymaže záznam podle id
     * @param int $id Id řádku v tabulce, který bude vymazán
     * @return int Počet ovlivněných řádků (1==úspěšná, 0==neúspěšná, >1==hackli nás...)
     */
    public function delete($id)
    {

        return $this->database->table(self::TABLE_NAME)->where(
            self::COLUMN_ID, $id
        )->delete();
    }

    /**
     * Vrátí seznam řádků (emailů a jejich zpráv) z tabulky TABLE_NAME v databázi
     * @return Nette\Database\Table\Selection seznam řádků
     */
    public function getAllRows() //:array
    {
        return $this->database->table(self::TABLE_NAME)->order(self::COLUMN_ID . ' DESC')->fetchAll();
    }
}