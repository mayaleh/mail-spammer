<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 22.5.18
 * Time: 11:14
 */

namespace App\Forms;

use Nette;
use Nette\Application\UI;
use Nette\Application\UI\Form;

/**
 * Class AddForm komponenta formulář
 * @package App\Forms
 */
class AddForm extends UI\Control
{
    use Nette\SmartObject;

    protected $values;

    public $onSubmit;

    /**
     * Vytvoření formuláře
     * @return Form
     */
    public function create()
    {
        $form = new Form;
        $form->addText('email', 'Emailová adresa')->setType('email')->setRequired()->addRule(Form::EMAIL, "Musíte vyplnit validní email.");
        $form->addTextArea('zprava', 'Zpráva')->setRequired()->addRule(Form::MIN_LENGTH, 'Zpráva musí být minimálně %d znaků dlouhá.', 10);
        $form->addSubmit('submit', 'Uložit');
        $form->onSuccess[] = [$this, 'processForm'];

        return $form;
    }

    /**
     * Zpracování formuláře
     * @param $form
     */
    public function processForm($form)
    {

    }
}