<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 24.5.18
 * Time: 16:40
 */

namespace App\Presenters;

use App;
use App\Model\EmailManager;
use App\Model\UrlManager;
use ErrorException;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;
use Nette\Database\Table\Selection;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Mail\SmtpException;
use Nette\Utils\ArrayHash;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

/**
 * Class MailerPresenter třída modelu pro správu tabulky sm_emails
 * @package App\Presenters
 */
class MailerPresenter extends BasePresenter
{
    const
        EMAIL_FROM = "salim@rtsoft.cz",
        NAME_FROM = "Spammer",
        SUBJECT_FROM = "SPAM";

    /** @var IMailer @inject */
    public $mailer;

    /** @var EmailManager Instance třídy modelu pro práci s emailovou tabulkou */
    protected $emailManager;

    /** @var UrlManager Instance třídy modelu pro práci s url tabulkou */
    protected $urlManager;

    private $allEmails, $jsonData, $dataUrl;

    /**
     * MailerPresenter constructor s injektovanou třídou pro práci s modelem.
     * @param EmailManager $emailManager automaticky injektovaná třída pro práci s modelem (tabulka sm_emails)
     * @param UrlManager $urlManager automaticky injektovaná třída pro práci s modelem (tabulka sm_url)
     */
    public function __construct(EmailManager $emailManager, UrlManager $urlManager)
    {
        parent::__construct();
        $this->emailManager = $emailManager;
        $this->urlManager = $urlManager;
    }


    public function renderDefault(){
        // nic
    }

    /**
     * Zavolá odesílání emaulů, předá jí potřebné parametry (řádky z tabulky)
     * @param $id ?|string očekává se hash URL předaným presenterem
     * @throws ErrorException |
     * @throws BadRequestException |
     * @void
     */
    public function actionSend($id)
    {
        if (empty($id)) throw new BadRequestException();


        $this->dataUrl = $this->urlManager->getValidUrl($id);

        if ($this->dataUrl <= 0){
            throw new BadRequestException();
        }else{
            if (($allEmails = $this->emailManager->getAllRows()) === false) throw new ErrorException("Row not found.");
            $this->allEmails = $allEmails;
            $this->mailator($this->allEmails);
        }


    }

    /**
     * Vytvoří JSON resul a pošle ho do view template
     * @throws AbortException | @void vyjímka při selhání generování JSON
     */
    public function renderSend()
    {
        try {
            $this->sendJson(Json::encode($this->jsonData, Json::PRETTY));
            //$this->template->jsonInfo = Json::encode($this->jsonData, Json::PRETTY);
            //$this->redirect("Homepage:default");
        } catch (JsonException $e) {
            $this->template->jsonInfo = "JSON ERROR: Encode failed.";
        }
    }

    /**
     * @param $mailsData Selection řádky z tabulky
     * @void
     */
    private function mailator($mailsData)
    {
        foreach ($mailsData as $item) {
            $this->sendMail($item);
        }
    }

    /**
     * Odešle email pomocí SMTP vytvořeného DI kontejnerem a nastaví JSON result
     * @param $args Selection řádek id, email, zpráva
     * @void
     */
    protected function sendMail($args)
    {
        try{
            $emailFrom = self::NAME_FROM . ' <' . self::EMAIL_FROM . '>';
            $mail = new Message;
            $mail->setFrom($emailFrom)
                ->addTo($args->email)
                ->setSubject(self::SUBJECT_FROM)
                ->setBody($args->zprava);

            $this->mailer->send($mail);

            $this->jsonData['SUCCESS'][] = array(
                'id' => $args->id,
                'email' => $args->email,
                'zprava' => $args->zprava,
            );
        } catch (SmtpException $e ) {
            $this->jsonData['FAILED'][] = array(
                'email' => $args->email,
                'zprava' => $args->zprava,
                'error' => 'SMTP connection error'
            );
            //throw $e;
        }
    }
}