<?php

namespace App\Presenters;

use App;
use App\Model\EmailManager;
use App\Forms\AddForm;
use ErrorException;
use Nette\Application\AbortException;
use Nette\Application\UI;
use Nette\Application\UI\Form;
use Nette\InvalidStateException;
use Nette\Utils\ArrayHash;

/**
 * Class HomepagePresenter Controler úvodního načtení
 * @package App\Presenters
 */
class HomepagePresenter extends BasePresenter
{
    /** @var EmailManager Instance třídy modelu pro práci s emailovou tabulkou */
    protected $emailManager;

    /** @var App\Forms\AddForm @inject */
    public $addFormFactory;

    /**
     * Konstruktor s injektovaným modelem pro práci s emailovou tabulkou
     * @param EmailManager $emailManager automaticky injektovaná třída modelu pro práci s články
     */
    public function __construct(EmailManager $emailManager)
    {
        parent::__construct();
        $this->emailManager = $emailManager;
    }

    /**
     * získá všchny záznamy z tabulky mailů
     * @void
     * @throws ErrorException
     */
    public function renderDefault()
    {
        if (($allEmails = $this->emailManager->getAllRows()) === false) throw new ErrorException("Row not found.");
        $this->template->allEmails = $allEmails; // Předá do šablony.
    }


    /**
     * @param $id int pro vymazání záznamu
     */
    public function actionDelete($id)
    {
        $this->emailManager->delete($id);
    }

    /**
     * přesměrování
     * @throws AbortException
     */
    public function renderDelete()
    {
        try {
            $this->redirect('default');
        } catch (AbortException $e) {
            throw new AbortException("Error: redirect to default");
        }
    }


    /**
     * Vytvoří fomulář jako komponentu s jejím zpracováním
     * @return Form
     */
    protected function createComponentAddForm()
    {
        $form = $this->addFormFactory->create();
        $form->onSuccess[] = function (UI\Form $form, $values) {
            try {
                $this->emailManager->add($values);
                /*
                $mail = new Message;
                $mail->setFrom($values->email)
                    ->addTo("salim@rtsoft.cz")
                    ->setSubject('Email z webu')
                    ->setBody($values->message);
                $mailer = new SendmailMailer;
                $mailer->send($mail);
                */

                $this->flashMessage('Email byl uložen.');
                $this->redirect('this');
            } catch (InvalidStateException $ex) {
                $this->flashMessage('Email se nepodařilo uložit.');
            }
        };

        return $form;
    }

}
