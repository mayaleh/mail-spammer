<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 24.5.18
 * Time: 8:39
 */

namespace App\Presenters;

use Nette;

/**
 * Základní presenter pro všechny ostatní presentery aplikace.
 * @package App\Presenters
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

}