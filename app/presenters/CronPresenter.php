<?php
/**
 * Created by PhpStorm.
 * User: Salim Mayaleh
 * Date: 5.6.18
 * Time: 12:40
 */

namespace App\Presenters;

use App\Model\UrlManager;
use Nette\Application\BadRequestException;

/**
 * CronPresenter pro spouštení cron úloh. Generování Hash Url
 * @package App\Presenters
 */
class CronPresenter extends BasePresenter
{
    /** @var UrlManager Instance třídy modelu pro práci s url tabulkou */
    protected $urlManager;

    private $dataLog;

    /**
     * CronPresenter constructor.
     * @param UrlManager $urlManager pro práci s tabulkou Url (sm_url)
     */
    public function __construct(UrlManager $urlManager)
    {
        parent::__construct();
        $this->urlManager = $urlManager;

    }

    /**
     * @throws BadRequestException
     */
    public function renderDefault(){
        throw new BadRequestException();
    }

    /**
     * @param $id ? heslo ulozene v cronu
     * @throws BadRequestException | @void
     */
    public function actionGen($id){
        if (empty($id)) throw new BadRequestException();
        if ($id != '3a3v55b8b8b9f85ssd47') throw new BadRequestException();
        $this->dataLog = 'Creating new url hash in ' . date("Y-m-d H:i") . ' STATUS:  ' . $this->urlManager->createNewUrl() . ' n/ ';

    }

    /**
     * Defaultní vykreslení - vypíše se pouze stav
     * @void
     */
    public function renderGen(){
        $this->template->log = $this->dataLog;
    }
}