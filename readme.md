Nette Email Spammer Project
=================

For saving and sending all saved emails.

[Nette](https://nette.org) is a popular tool for PHP web development.
It is designed to be the most usable and friendliest as possible. It focuses
on security and performance and is definitely one of the safest PHP frameworks.


Requirements
------------
Docker, Docker Compose, SMTP

Or

- Apache2
- PHP 7.0 or higher
- MySQL
- SMTP (your email and login smtp for sending emails)
- phpmyadmin (optional)



Docker Compose
------------
Go to root directory of your project and run docker compose:

    docker-compose up -d

or:

    docker-compose up
    
to see all processes, then open new command line.

Then, you have to setup connection to database in `app/config/config.neon` by adding:
```neon
database:
	dsn: 'mysql:host=database_container_name:3306;dbname=nette-test'
	user: 'root'
	password: 'root'
	options:
		lazy: yes
```
List database container name by command:

    docker container ls
    
or use database container IPAddress:

    docker container inspect db_container_name | grep '"IPAddress"'
    
    
It should be `127.x.x.x`

On Linux, don't forget to set directories `log/`, `temp/` and `.docker/nginx/log` writable (Use this only for your localhost):

    chmod 777 temp log .docker/nginx/log

Cron setting:

Log in to `nette-proxy` container (current name of your container find by command: `docker container ls` ):

    docker container exec -it <name_of_nette_proxy_container> bash
    
Now in commad line inside the container just type:

    cron
    
    
It wil strat cron. Cron ensure to execute script for generating new hash for url

Now, it should work, open in your browser `http://localhost`



Features
--------------
Saving emails and messages to database.

List saved emails and messages.

Delete emails.

Sending all saved emails:
go to URL: `http://localhost/mailer/send/<latest generated hash>`
It will return JSON result.


Installation Nette
------------

The best way to install Web Project is using Composer. If you don't have Composer yet,
download it following [the instructions](https://doc.nette.org/composer). Then use command:

	composer create-project nette/web-project path/to/install
	cd path/to/install



